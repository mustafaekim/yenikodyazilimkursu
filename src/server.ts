
import * as path from "path";
import * as express from 'express'
import * as logger from "morgan";
import * as nunjucks from "nunjucks";
import * as http from "http";


export class Server {

  public ROOT = path.join(path.resolve(__dirname, '../'));
  public app: express.Application;
  public server: http.Server;

  constructor(port: number) {
    this.app = express()
    this.templating();
    this.routes();

    this.server = this.app.listen(port, function () {
      console.log(`Yenikod Yazılım Kursu Web App is listening on port ${port}!`);
    });
  }

  private templating() {
    //this.app.set("views", path.join(__dirname, "views"));

    nunjucks.configure('view', {
      autoescape: true,
      express: this.app,
      noCache: true,
      tags: {
        //blockStart: '<%',
        //blockEnd: '%>',
        variableStart: '<%',
        variableEnd: '%>',
        //commentStart: '<#',
        //commentEnd: '#>'
      }
    });
  }

  private routes(): void {
    this.app.use(logger("dev"));

    this.app.use('/static', express.static(path.resolve(this.ROOT, 'static')));
    this.app.use('/node_modules', express.static(path.resolve(this.ROOT, 'node_modules')));
    this.app.use('/', express.static(path.resolve(this.ROOT, 'www')));

    const indexRouter: express.Router = express.Router();

    /* Demo */


    function printStars(n: number) {
      var text = "";
      for (var innerCounter = 0; innerCounter < n; innerCounter++) {
        text = text + "*";
      }
      return text;
    }

    indexRouter.get("/demo/1/:magicNumber", function (req, res) {
      res.send(`
      <html>
        <body>
          <h1>Hello World!</h1>
          <div style="text-align:center; margin:15px; background:#eee; padding:15px; border:1px dashed blue;">
            My name is <b>Mustafa!</b>
          </div>
          <div style="color:${req.params.magicNumber%2==0 ? 'blue' : 'red'}">
          <p>${printStars(req.params.magicNumber)}</p>
        </body>
      </html>
      `
      );
    })




    /* Anasayfa */
    indexRouter.get('/', function (req, res) {
      res.render('courses.html', { page: "courses", title: "Hızlandırılmış Yazılım Kursu: Programlamanın ve Kodlamanın Temelleri" });
    });

    /* 3 aylık kurs */
    indexRouter.get('/hizlandirilmis-yogun-yazilim-kursu', function (req, res) {
      res.render('coding-bootcamp.html', { page: "courses", title: "3 Aylık Yazılım Kursu: Hızlandırılmış ve Yoğun Program" });
    });

    /* 101 */
    indexRouter.get('/yeni-baslayanlar-icin-yazilim-kursu', function (req, res) {
      res.render('101.html', { page: "courses", title: "Yeni Başlayanlar İçin Sıfırdan Yazılım Kursu: JavaScript, CSS, HTML, SQL" });
    });

    /* 102 */
    indexRouter.get('/design-pattern-mimariler-javascript-typescript-es6-yazilim-kursu', function (req, res) {
      res.render('102.html', { page: "courses", title: "Design Pattern, Prensip ve Mimariler Yazılım Kursu: EcmaScript (ES6), TypeScript, NodeJS" });
    });

    /* 103 */
    indexRouter.get('/modern-spa-web-uygulamalari-express-nodejs-angular-yazilim-kursu', function (req, res) {
      res.render('103.html', { page: "courses", title: "NodeJS, Express ve Angular Yazılım Kursu: Modern SPA Web Uygulamaları" });
    });

    /* Eğitmen */
    indexRouter.get('/yazilim-egitmeni', function (req, res) {
      res.render('instructor.html', { page: "instructor", title: "Yazılım Kursu Eğitmeni: Mustafa Ekim" });
    });

    /* İletişim */
    indexRouter.get('/yazilim-kursu-iletisim', function (req, res) {
      res.render('contactus.html', { page: "contactus" });
    });

    /* Yazılar */
    indexRouter.get('/yazilim-egitimi', function (req, res) {
      res.render('articles.html', { page: "articles" });
    });

    /* Yazı: Yazılımda kariyer */
    indexRouter.get('/yazilim-sektorunun-sundugu-kariyer-firsatlari', function (req, res) {
      res.render('coding-career.html', { page: "articles" });
    });

    /* Yazı: Kimler iyi yazılımcı olabilir */
    indexRouter.get('/kimler-iyi-yazilimci-olabilir-kodlama-ogrenebilir-miyim', function (req, res) {
      res.render('who-can-code.html', { page: "articles" });
    });

    /* Yazı: Yazılım nasıl öğrenilir */
    indexRouter.get('/yazilim-nasil-ogrenilir-kodlama-ogrenmek', function (req, res) {
      res.render('how-to-learn-coding.html', { page: "articles" });
    });

    /* Yazı: Neden Hızlandırılmış Yoğun Yazılım Eğitimi */
    indexRouter.get('/neden-hizlandirilmis-yogun-ozel-yazilim-kursu', function (req, res) {
      res.render('why-bootcamp.html', { page: "articles" });
    });

    /* Yazılımdaki Trendler */
    indexRouter.get('/yazilimdaki-trendler-2017', function (req, res) {
      res.render('software-dev-trends-2017.html', { page: "articles" });
    });

    /* Çocuklara Yazılım Eğitimi */
    indexRouter.get('/cocuklara-kodlama-egitimi-yazilima-erken-yasta-baslamak', function (req, res) {
      res.render('coding-course-for-children.html', { page: "articles" });
    });

    /* İngilizce Yazılım Eğitimi */
    indexRouter.get('/ingilizce-yazilim-egitimi-kodlama', function (req, res) {
      res.render('english-coding-course.html', { page: "articles" });
    });

    this.app.use('/', indexRouter)
  }

}